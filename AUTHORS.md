Authors
=======
We'd like to thank the following people for their contributions:


- Adolfo Jayme Barrientos \<fitojb⒜ubuntu∙com\>
- Albynton \<albynton⒜gmail∙com\>
- Alexander Schmidt \<alexander42schmidt⒜gmx∙de\>
- Alexandr Eremeev \<ae125529⒜gmail∙com\>
- Alex \<tminei⒜ukr∙net\>
- Allan Nordhøy \<epost⒜anotheragency∙no\>
- Andreas Redmer \<ar-appleflinger⒜abga∙be\>
- Arttu Ylhävuori \<arjymoi⒜hotmail∙com\>
- ButterflyOfFire \<ButterflyOfFire⒜protonmail∙com\>
- DeMmAge \<hgt166782⒜gmail∙com\>
- Dmitry Mikhirev \<mikhirev⒜gmail∙com\>
- Grzegorz Sagadyn \<grzegorz∙sagadyn⒜speednet∙pl\>
- Heimen Stoffels \<vistausss⒜outlook∙com\>
- Iván Seoane \<ivanrsm1997⒜gmail∙com\>
- Iváns \<ivanrsm1997⒜gmail∙com\>
- JC Staudt \<jc⒜staudt∙io\>
- John Doe \<dansroot⒜yadim∙dismail∙de\>
- Jonatan Nyberg \<jonatan∙nyberg∙karl⒜gmail∙com\>
- leela \<53352⒜protonmail∙com\>
- Louies \<louies0623⒜gmail∙com\>
- Mahtab Alam \<mahtab∙ceh⒜gmail∙com\>
- Manuela Silva \<mmsrs⒜sky∙com\>
- Markel \<wakutiteo⒜protonmail∙com\>
- Mattias Münster \<mattiasmun⒜gmail∙com\>
- Milo Ivir \<mail⒜milotype∙de\>
- Oguz Ersen \<oguzersen⒜protonmail∙com\>
- Osoitz \<oelkoro⒜gmail∙com\>
- Rex_sa \<asd1234567890m⒜gmail∙com\>
- Rui Mendes \<xz9⒜protonmail∙com\>
- Sérgio Marques \<smarquespt⒜gmail∙com\>
- urain39 \<hexiedeshijie⒜gmail∙com\>
- Verdulo \<tomek⒜disroot∙org\>
- WaldiS \<admin⒜sto∙ugu∙pl\>
- xin \<xin⒜riseup∙net\>
- xxssmaoxx \<simon∙dottor⒜gmail∙com\>
- YONGLE \<tanyongle0633⒜gmail∙com\>
- Сухичев Михаил Иванович \<sukhichev⒜yandex∙ru\>
